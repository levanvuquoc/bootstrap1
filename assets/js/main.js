
// ========  Pace Loading =========
    paceOptions = {
      elements: true
    };
   
// ======== Shrink navbar =========
$(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('.header-dark').addClass('shrink');
    $('.scroll-to-top').fadeIn();
  } else {
    $('.header-dark').removeClass('shrink');
    $('.scroll-to-top').fadeOut();
  }
});

// ========  Active navbar =========
$(document).ready(function () {
  $(document).on("scroll", onScroll);
  
  //smoothscroll
  $('a[href^="#"]').on('click', function (e) {
      e.preventDefault();
      $(document).off("scroll");
      
      $('a').each(function () {
          $(this).removeClass('active');
      })
      $(this).addClass('active');
    
      var target = this.hash,
          menu = target;
      $target = $(target);
      $('html, body').stop().animate({
          'scrollTop': $target.offset().top
      }, 500, 'swing', function () {
          window.location.hash = target;
          $(document).on("scroll", onScroll);
      });
  });
});

function onScroll(event){
  var scrollPos = $(document).scrollTop();
  $('#navigation a').each(function () {
      var currLink = $(this);
      var refElement = $(currLink.attr("href"));
      if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
          $('#menu-center ul li a').removeClass("active");
          currLink.addClass("active");
      }
      else{
          currLink.removeClass("active");
      }
  });
}

// ========  Carousel =========
 /** Carousel Custom - Init */
 if ($.fn.flickity){
            
  /** Section - About*/
  var $carouselAbout = $('#about').find('.carousel-custom');
  carouselCustom($carouselAbout);
  
  /** Section - Testimonial*/
  var $carouselTestimonial = $('#carousel-testimonial');
  carouselCustom($carouselTestimonial);
}

/** Carousel Custom */
function carouselCustom($elem){
  var $carouselControl = $elem.closest('.carousel-custom-wrap').find('.carousel-custom-control'),
      $btnPrev = $carouselControl.find('.control-previous'),
      $btnNext = $carouselControl.find('.control-next');
      
  $elem.flickity({
      cellSelector: '.carousel-cell',
      cellAlign: 'left',
      contain: true,
      prevNextButtons: false,
      pageDots: $elem.data('pagedots'),
      draggable: $elem.data('draggable'),
      autoPlay: $elem.data('autoplay'),
      imagesLoaded: true,
      pauseAutoPlayOnHover: false
  });
  
  var flkty = $elem.data('flickity');
  $elem.find('.flickity-page-dots').on('mouseleave', function(){ 
      flkty.playPlayer(); 
  });
  
  $btnPrev.on('click', function(e){
      $elem.flickity('previous', true);
      e.preventDefault();
  });
  
  $btnNext.on('click', function(e){
      $elem.flickity('next', true);
      e.preventDefault();
  });
}

// ========  Clock Countdown =========
if ($.fn.countdown){
  var $clock = $('#clock'),
      untilDate = $clock.data('until-date');

  $clock.countdown(untilDate, function(e){
      $(this).html(e.strftime(''
          + '<div class="clock-item border-base-color border-bottom border-medium-thick d-inline-block mx-3 opacity-9-5 pb-3 px-3 px-sm-4 text-white"><div class="d-table h-100 w-100"><div class="d-table-cell align-middle text-center"><span class="d-block font-alt text-xs-large title-sm-medium title-extra-large-3">%D</span><span class="d-block font-alt letter-spacing-2 mt-3 text-uppercase text-sm-small title-medium">Days</span></div></div></div>'
          + '<div class="clock-item border-base-color border-bottom border-medium-thick d-inline-block mx-3 opacity-9-5 pb-3 px-3 px-sm-4 text-white"><div class="d-table h-100 w-100"><div class="d-table-cell align-middle text-center"><span class="d-block font-alt text-xs-large title-sm-medium title-extra-large-3">%H</span><span class="d-block font-alt letter-spacing-2 mt-3 text-uppercase text-sm-small title-medium">Hr</span></div></div></div>'
          + '<div class="clock-item border-base-color border-bottom border-medium-thick d-inline-block mt-4 mx-3 opacity-9-5 pb-3 px-3 px-sm-4 text-white"><div class="d-table h-100 w-100"><div class="d-table-cell align-middle text-center"><span class="d-block font-alt text-xs-large title-sm-medium title-extra-large-3">%M</span><span class="d-block font-alt letter-spacing-2 mt-3 text-uppercase text-sm-small title-medium">Min</span></div></div></div>'
          + '<div class="clock-item border-base-color border-bottom border-medium-thick d-inline-block mt-4 mx-3 opacity-9-5 pb-3 px-3 px-sm-4 text-white"><div class="d-table h-100 w-100"><div class="d-table-cell align-middle text-center"><span class="d-block font-alt text-xs-large title-sm-medium title-extra-large-3">%S</span><span class="d-block font-alt letter-spacing-2 mt-3 text-uppercase text-sm-small title-medium">Sec</span></div></div></div>'));
  });
}

// ========  Countnumber =========
var options = {
      useEasing: true, 
      useGrouping: true, 
      separator: ',', 
      decimal: '.', 
    };
    var demo = new CountUp('timer', 0, 2780, 0, 1.2, options);
    if (!demo.error) {
          demo.start();
        } else {
          console.error(demo.error);
        }
    var demo = new CountUp('timer1', 0, 80, 0, 1.2, options);
    if (!demo.error) {
      demo.start();
    } else {
      console.error(demo.error);
    }
    var demo = new CountUp('timer2', 0, 13730, 0, 1.2, options);
    if (!demo.error) {
      demo.start();
    } else {
      console.error(demo.error);
    }
    var demo = new CountUp('timer3', 0, 154, 0, 1.2, options);
    if (!demo.error) {
      demo.start();
    } else {
      console.error(demo.error);
    }

// ========  Chart =========
if ($.fn.easyPieChart){
    var $pieChart = $('.pie-chart').find('.chart');
    $pieChart.easyPieChart({
        scaleColor: false,
        lineCap: 'butt',
        lineWidth: 10,
        size: 210,
        animate: 2000,
        easing: 'easeOutBounce',
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent) + '%');
        }
    });
    
    $pieChart.one('inview', function(isInView){
        if(isInView){
            $(this).data('easyPieChart').update($(this).data('percent-update'));
        }
    });
}


if ($.fn.imagesLoaded && $.fn.isotope){
    $('.gallery-grid').imagesLoaded(function(){
        $('.gallery-grid').isotope({
            itemSelector: '.item',
            layoutMode: 'masonry'
        });
    });
}


/** Gallery filtering */
if ($.fn.imagesLoaded && $.fn.isotope){
    var $gridSelectors = $('.gallery-filter').find('a');
    $gridSelectors.on('click', function(e){
        var selector = $(this).attr('data-filter');
        $('.gallery-grid').isotope({
            filter: selector
        });            
        
        $gridSelectors.removeClass('disabled');
        $(this).addClass('disabled');
        
        e.preventDefault();
    });
}


if ($.fn.magnificPopup){
    $('.gallery-grid').magnificPopup({
        delegate: 'a',
        type: 'image',
        mainClass: 'mfp-fade',
        gallery:{
            enabled: true,
            navigateByImgClick: true,
            preload: [0,2],
            tPrev: 'Previous',
            tNext: 'Next',
            tCounter: '<span class="mfp-counter-curr">%curr%</span> of <span class="mfp-counter-total">%total%</span>'
        }
    });
    
    var $popupTrigger = $('.popup-trigger'),
        $popupTriggerClose = $('.popup-trigger-close');

    $popupTrigger.on('click', function(e){
        $.magnificPopup.open({
            items: {
                src: $(this).closest('.popup-container').find('.popup-content')
            },
            type: 'inline',
            fixedContentPos: true,
            closeOnContentClick: false,
            callbacks: {
                open: function () {
                    $('.mfp-wrap').addClass('popup-wrap');
                },
                close: function () {
                    $('.mfp-wrap').removeClass('popup-wrap');
                }
            }
        });
        
        e.preventDefault();
    });
    
    $popupTriggerClose.on('click', function(e){
        $.magnificPopup.close();
        e.preventDefault();
    });
}